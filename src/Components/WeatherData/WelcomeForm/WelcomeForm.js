import React from 'react';
import './WelcomeForm.css';

const WelcomeForm = props => {
  return(
    <div className='WelcomeForm'>
      <h1>Welcome to the app</h1>
      <div className='Description'>
        <h3>Here you can know the weather of a certain place on earth</h3>
        <p>To do this, you need to enter the "latitude" and "longitude"</p>
      </div>
      <div className='EnterDataForm'>
        <input onChange={props.latitude}
               type="text"
               placeholder='Enter latitude'/>
        <input onChange={props.longitude}
               type="text"
               placeholder='Enter longitude'/>
      </div>
      <button className='CheckBtn'
              onClick={props.click}>
        Check the weather
      </button>
    </div>
  );
};

export default WelcomeForm;