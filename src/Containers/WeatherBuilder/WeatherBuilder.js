import React, {Component} from 'react';
import axios from 'axios';
import WeatherData from "../../Components/WeatherData/WeatherData";
import './WeatherBuilder.css';
import WelcomeForm from "../../Components/WeatherData/WelcomeForm/WelcomeForm";

const BASE_URL = '/forecast/ef675ff61bb10a80f2da0a82f44cb674/';

class WeatherBuilder extends Component {

  state = {
    weather: [],
    latitude: 0,
    longitude: 0,
    showCurrently: false,
  };

  enterLatitude = (event) => {
    let latitude = event.target.value;

    this.setState({latitude});
  };

  enterLongitude = (event) => {
    let longitude = event.target.value;

    this.setState({longitude});
  };

  checkTheWeather = () => {
    if (this.state.latitude && this.state.longitude !== 0) {
      axios.get(BASE_URL + this.state.latitude + ',' + this.state.longitude)
        .then(weather => {
          this.setState({weather: weather.data})
        })
    }
  };

  checkTheCurrentlyWeather = () => {
    this.setState({showCurrently: true});
  };

  render() {
    return(
      <div className='WeatherBuilder'>
        <WelcomeForm click={this.checkTheWeather}
                     longitude={(event) => this.enterLongitude(event)}
                     latitude={(event) => this.enterLatitude(event)}/>
        <WeatherData weather={this.state.weather}
                     show={this.state.showCurrently}
                     click={this.checkTheCurrentlyWeather}/>
      </div>
    );
  }
}

export default WeatherBuilder;